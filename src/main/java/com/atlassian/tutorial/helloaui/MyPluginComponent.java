package com.atlassian.tutorial.helloaui;

public interface MyPluginComponent
{
    String getName();
}